package com.springbooth2.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springbooth2.demo.dao.*;
import com.springbooth2.demo.Model.*;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeRepository repository;
	
	
	//search, update and delete operations
	@RequestMapping(value="/employees",method =RequestMethod.GET)
	List<Employee> getEmployee() {
		return repository.findAll();
		
	}
	@RequestMapping(value="/employee/{id}",method =RequestMethod.GET)
	Employee getEmployee(@PathVariable Integer id) {
		return repository.findById(id).get();
		
	}
	
	@RequestMapping (value="/employees/", method= RequestMethod.POST)
	String addEmployees(@RequestBody List<Employee> emplist) {
		 repository.saveAll( emplist);
		return "Success";
		
	}
	
	 
	
	
	
	@RequestMapping (value="/employees",method=RequestMethod.PUT)
String updateEmployee(@RequestBody List<Employee> employee) {
		 repository.saveAll(employee);
		 return "SUCCESS";
		
		
	}
	
	@RequestMapping (value="/employees",method=RequestMethod.DELETE)
	String deleteAllEmployee() {
		repository.deleteAll();
		return "Success";
		
	}
}
